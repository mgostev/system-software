#include <iostream>
#include <pthread.h>
#include <unistd.h>
#include <sstream>
#include <vector>

class Value {
public:
    Value() = default;

    void update(const int value) {
        _value = value;
    }

    [[nodiscard]] int get() const {
        return _value;
    }

private:
    int _value = 0;
};

pthread_mutex_t global_mutex = PTHREAD_MUTEX_INITIALIZER;

pthread_cond_t cond_consumers_started = PTHREAD_COND_INITIALIZER;
pthread_cond_t cond_producer_signal = PTHREAD_COND_INITIALIZER;
pthread_cond_t cond_operation_performed = PTHREAD_COND_INITIALIZER;

pthread_t producer_thread;
pthread_t *consumer_threads_ptr;
pthread_t interruptor_thread;

class RuntimeState {
public:
    mutable volatile bool debug = false;
    mutable volatile int consumer_count = -1;
    mutable volatile int max_sleep_time = -1;

    mutable volatile int answer = 0;
    mutable volatile bool stream_finished = false;
    mutable volatile int consumer_working = 0;
    mutable volatile int consumer_finihed = 0;
};

const RuntimeState runtime_state;

pthread_key_t tid_addr_key;
void key_create() { pthread_key_create(&tid_addr_key, free); }
int get_tid() {
    // 1 to 3+N thread ID
    static int next_tid = 1;
    static pthread_once_t key_created = PTHREAD_ONCE_INIT;
    pthread_once(&key_created, key_create);

    int const *key_ptr = static_cast<const int *>(pthread_getspecific(tid_addr_key));
    if (key_ptr == nullptr) {
        int *id_ptr = static_cast<int *>(malloc(sizeof(int)));
        *id_ptr = next_tid++;
        pthread_setspecific(tid_addr_key, id_ptr);
        return *id_ptr;
    }

    return *key_ptr;
}

void *producer_routine(void *arg) {
    // Wait for consumer to consumer_working
    // Read data, loop through each value and update the value, notify consumer, wait for consumer to process
    auto *value_ptr = static_cast<Value *>(arg);

    auto buffer = std::string{};
    std::getline(std::cin, buffer);
    std::istringstream ss(buffer);
    std::vector<int> numbers;

    int inputNumber;
    while (ss >> inputNumber){
        numbers.push_back(inputNumber);
    }

    pthread_mutex_lock(&global_mutex);

    for (auto number: numbers){
        while (value_ptr->get() != 0 || runtime_state.consumer_working == 0) {
            pthread_cond_wait(&cond_operation_performed, &global_mutex);
        }

        value_ptr->update(number);

        pthread_cond_signal(&cond_producer_signal);
    }

    while (value_ptr->get() != 0 || runtime_state.consumer_working == 0) {
        pthread_cond_wait(&cond_operation_performed, &global_mutex);
    }

    runtime_state.stream_finished = true;
    value_ptr->update(42);
    pthread_mutex_unlock(&global_mutex);

    while (runtime_state.consumer_finihed < runtime_state.consumer_count) {
        pthread_mutex_lock(&global_mutex);
        pthread_cond_broadcast(&cond_producer_signal);
        pthread_mutex_unlock(&global_mutex);
    }

//    pthread_mutex_unlock(&global_mutex);

    pthread_exit(nullptr);
}

void *consumer_routine(void *arg) {
    // notify about consumer_working
    // for every update issued by producer, read the value and add to sum
    // return pointer to result (for particular consumer)
    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, nullptr);

    auto *value_ptr = static_cast<Value *>(arg);
    int local_sum = 0;

    while (true) {

        pthread_mutex_lock(&global_mutex);
        if (runtime_state.stream_finished) {
            runtime_state.consumer_finihed++;
            pthread_cond_signal(&cond_operation_performed);
            pthread_mutex_unlock(&global_mutex);
            break;
        }

        runtime_state.consumer_working++;
        pthread_cond_signal(&cond_operation_performed);

        do {
            pthread_cond_wait(&cond_producer_signal, &global_mutex);
        } while (value_ptr->get() == 0);


        if (!runtime_state.stream_finished) {
            const int consumed_number = value_ptr->get();
            local_sum += consumed_number;
            runtime_state.answer += consumed_number;
            if (runtime_state.debug) {
                std::cout << '(' << get_tid() << ", " << local_sum << ")\n";
            }

            value_ptr->update(0);
            pthread_cond_signal(&cond_operation_performed);
        }

        runtime_state.consumer_working--;
        pthread_mutex_unlock(&global_mutex);

        if (runtime_state.max_sleep_time > 0 && !runtime_state.stream_finished) {
            useconds_t sleep_time = 1000 * (random() % runtime_state.max_sleep_time);
            usleep(sleep_time);
        }
    }

    pthread_exit(nullptr);
}

void *consumer_interruptor_routine(void *arg) {
    // wait for consumers to consumer_working
    // interrupt random consumer while producer is running
    if (arg) {}

    while (!runtime_state.stream_finished) {
        int working = runtime_state.consumer_working;
        if (working > 0) {
            int interrupted_consumer_idx = (int) (random() % working);
            pthread_cancel(consumer_threads_ptr[interrupted_consumer_idx]);
        }
    }

    pthread_exit(nullptr);
}

void parse_args(int argc, char **argv) {
    if (argc < 3) {
        exit(1);
    }

    for (int i = 1; i < argc; ++i) {
        auto arg = std::string(argv[i]);
        if (arg == std::string("-debug")) {
            runtime_state.debug = true;
            continue;
        }

        if (runtime_state.consumer_count == -1) {
            runtime_state.consumer_count = (int) strtol(argv[i], nullptr, 10);
            continue;
        }

        if (runtime_state.max_sleep_time == -1) {
            runtime_state.max_sleep_time = (int) strtol(argv[i], nullptr, 10);
            continue;
        }

        exit(1);
    }

    if (runtime_state.consumer_count <= 0) {
        exit(1);
    }
    if (runtime_state.max_sleep_time < 0) {
        exit(1);
    }
}

int run_threads() {
    // consumer_working N threads and wait until they're done
    // return aggregated sum of values

    pthread_mutex_init(&global_mutex, nullptr);
    pthread_cond_init(&cond_producer_signal, nullptr);
    pthread_cond_init(&cond_operation_performed, nullptr);

    auto *shared_atomic_var = new Value();

    pthread_create(&producer_thread, nullptr, producer_routine, shared_atomic_var);
    pthread_create(&interruptor_thread, nullptr, consumer_interruptor_routine, shared_atomic_var);

    consumer_threads_ptr = static_cast<pthread_t *>(malloc(runtime_state.consumer_count * sizeof(pthread_t)));
    for (int i = 0; i < runtime_state.consumer_count; ++i) {
        pthread_create(&consumer_threads_ptr[i], nullptr, consumer_routine, shared_atomic_var);
    }

    pthread_join(producer_thread, nullptr);
    pthread_join(interruptor_thread, nullptr);

    for (int i = 0; i < runtime_state.consumer_count; ++i) {
        pthread_join(consumer_threads_ptr[i], nullptr);
    }
    free(consumer_threads_ptr);

    pthread_mutex_destroy(&global_mutex);
    pthread_cond_destroy(&cond_consumers_started);
    pthread_cond_destroy(&cond_producer_signal);
    pthread_cond_destroy(&cond_operation_performed);

    delete shared_atomic_var;

    return runtime_state.answer;
}

int main(int argc, char **argv) {
    parse_args(argc, argv);
    std::cout << run_threads() << std::endl;
    return 0;
}
