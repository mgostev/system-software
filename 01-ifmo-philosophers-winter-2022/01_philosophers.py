from threading import Lock

PHILOSOPHERS_AMOUNT = 5

class DiningPhilosophers:
    
    fork_locks = [Lock() for _ in range(PHILOSOPHERS_AMOUNT)]
    
    def wantsToEat(self,
                   philosopher: int,
                   pickLeftFork: Callable[[], None],
                   pickRightFork: Callable[[], None],
                   eat: Callable[[], None],
                   putLeftFork: Callable[[], None],
                   putRightFork: Callable[[], None]) -> None:
        # Assign left and right philosopher's fork
        left_fork_idx = (philosopher - 1) % PHILOSOPHERS_AMOUNT
        right_fork_idx = philosopher
        
        # Prioritize forks by index (the less fork index the highest its priority)
        first_fork_idx = min(left_fork_idx, right_fork_idx)
        second_fork_idx = max(left_fork_idx, right_fork_idx)

        # Take locks in priority order
        with self.fork_locks[first_fork_idx]:
            with self.fork_locks[second_fork_idx]:
                pickLeftFork()
                pickRightFork()

                eat()

                putLeftFork()
                putRightFork()
